/*
 * Title: Implement suffix array algorithm in sorting completed strings
 * 
 * Author: Yan Chen -- Apirl 1st, 2013
 * Advisor: Andrzej Ehrenfeucht
 * 
 * Definition of completed string:
 * In a text T, if a substring C(occurrence in T is n) satisfies the following 2 conditions, then it is a completed string:
 * a) The pre-extention of C, xC where x is any single letter or null, occurs in T k times, and k<=n/2,
 * b) The suffix-extention of C, Cx where x is any single letter or null, occurs in T i times, and i<=n/2
 * 
 * Definition of identifier: 
 * In a complete string C(occurrence = n), if a substring S satisfies the following 2 conditions, then it is an identifier:
 * a) S only occurs once in C
 * b) S(occurrence in T is m) satisies n>m/2 <=> 2n>m
 * 
 * Some rules:
 * 1. the length of complete string has to be greater than 1
 * 2. if "ab" is an identifier and "abc" is one as well, then only print "ab"
 * 
 * input: txt format file
 * output: 'complete string'2/identifier1,2/identifier2,2/....
 * 
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.Scanner;
import java.io.InputStream;
import java.util.Comparator;

public class SuffixArray {
        private final String[] suffixes;
        private final int N;
        private final String[] test;
        static int counter;

        public SuffixArray(String s) {
                N = s.length();
                suffixes = new String[N];
                test = new String[N];
                for (int i = 0; i < N; i++) {
                        suffixes[i] = s.substring(i);
                        test[i] = s.substring(i);
                }
                Arrays.sort(suffixes);
        }

        // size of string
        public int length() {
                return N;
        }

        // index of ith sorted suffix
        public int index(int i) {
                return N - suffixes[i].length();
        }

        // ith sorted suffix
        public String select(int i) {
                return suffixes[i];
        }

        // number of suffixes strictly less than query
        public int rank(String query) {
                int lo = 0, hi = N - 1;
                while (lo <= hi) {
                        int mid = lo + (hi - lo) / 2;
                        int cmp = query.compareTo(suffixes[mid]);
                        if (cmp < 0)
                                hi = mid - 1;
                        else if (cmp > 0)
                                lo = mid + 1;
                        else
                                return mid;
                }
                return lo;
        }

        // length of longest common prefix of s and t
        private static int lcp(String s, String t) {
                int N = Math.min(s.length(), t.length());
                for (int i = 0; i < N; i++)
                        if (s.charAt(i) != t.charAt(i))
                                return i;
                return N;
        }

        // longest common prefix of suffixes(i) and suffixes(i-1)
        public int lcp(int i) {
                return lcp(suffixes[i], suffixes[i - 1]);
        }

        // longest common prefix of suffixes(i) and suffixes(j)
        public int lcp(int i, int j) {
                return lcp(suffixes[i], suffixes[j]);
        }

        static SuffixArray suffix;
        static String iden;
        static int identifier_count;

        /**
         * @param args
         * @throws IOException
         */
        public static void main(String[] args) throws IOException {

                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                String s = "/User/input.txt";
                iden = null;
                identifier_count = 0;
                FileReader f = new FileReader(s); // ~3MB.
                String string2;
                long startTime = System.currentTimeMillis();
                string2 = readWithString(f).toLowerCase();
                counter = 0;
                String text = string2.replaceAll("[^\\p{L}]", "_");//replace all non-letter symbol with underline "_"
                System.out.println(text);
                suffix = new SuffixArray(text);

                int N = text.length();
                System.out.println(N);
                String query, prequery, sufquery;
                prequery = null;
                sufquery = null;

                // find all occurrences of queries and give context
                int count1, count2, count3;
                int cu = 0;

                String outprint[][] = new String[200000][2];
                String temp_pre[] = new String[7000];
                String temp_suf[] = new String[7000];
                String temp_cur[] = new String[7000];
                temp_pre[0] = null;
                temp_suf[0] = null;
                temp_cur[0] = null;
                
                //Start finding the completed strings
                for (int c = 0; c < N - 1; c++) {

                        outerloop: for (int i = 2; i <= 30 && i <= N - c; i++) {
                                count1 = 0;
                                count2 = 0;
                                count3 = 0;
                                if ((c == 0) && (i == N))
                                        continue;

                                if ((c == N - 2) && (i == 2))
                                        continue;

                                query = text.substring(c, c + i);

                                for (int check = 0; check < cu; check++) {
                                        if (query.equals(outprint[cu][0]))
                                                continue outerloop;
                                }

                                count1 = 0;
                                for (int k = suffix.rank(query); k < N
                                                && suffix.select(k).startsWith(query); k++) {
                                        int indd = N - suffix.select(k).length();

                                        if (indd == 0) {
                                                temp_pre[count1] = "nothin more";
                                                temp_suf[count1] = text.substring(indd, indd + 1
                                                                + query.length());

                                                count1++;

                                                continue;
                                        }

                                        if (indd == N - query.length()) {
                                                temp_pre[count1] = text.substring(indd - 1, indd
                                                                + query.length());
                                                temp_suf[count1] = "nothin more";

                                                count1++;

                                                continue;
                                        }

                                        temp_pre[count1] = text.substring(indd - 1,
                                                        indd + query.length());
                                        temp_suf[count1] = text.substring(indd,
                                                        indd + 1 + query.length());

                                        count1++;
                                }

                                for (int t = 0; t < count1; t++) {
                                        count2 = 0;
                                        count3 = 0;
                                        for (int k = suffix.rank(temp_pre[t]); k < N
                                                        && (suffix.select(k).startsWith(temp_pre[t])); k++) {
                                                count2++;
                                        }
                                        for (int k = suffix.rank(temp_suf[t]); k < N
                                                        && (suffix.select(k).startsWith(temp_suf[t])); k++) {
                                                count3++;
                                        }

                                        if ((count2 > (count1 / 2)) || ((count3) > (count1 / 2)))
                                                continue outerloop;

                                }
                                if ((count1 > 2)) {
                                        if (cu == 0) {
                                                outprint[cu][0] = query;
                                                outprint[cu][1] = Integer.toString(count1);
                                                cu++;
                                                continue;
                                        }

                                        outprint[cu][0] = query;
                                        outprint[cu][1] = Integer.toString(count1);
                                        cu++;

                                }
                        }

                }

                long endTime = System.currentTimeMillis();
                System.out.println(" size " + N + " total time in second: "
                                + (endTime - startTime) / 1000.000 + " counter " + counter);
                String outprint1[][] = new String[cu][2];
                System.out.println(cu);

                for (int cc = 0; cc < cu; cc++) {
                        outprint1[cc][0] = outprint[cc][0];
                        outprint1[cc][1] = outprint[cc][1];

                }

                Arrays.sort(outprint1, new Comparator<String[]>() {
                        @Override
                        public int compare(final String[] entry1, final String[] entry2) {
                                final String time1 = entry1[0];
                                final String time2 = entry2[0];
                                return time1.compareTo(time2);
                        }
                });
                int index = 0;

                for (int cc = 0; cc < cu - 1; cc++) {

                        if (cc == 0) {

                                index++;
                                continue;
                        }

                        if (!outprint1[cc + 1][0].equals(outprint1[cc][0])) {

                                index++;
                        }
                }
                System.out.println(index);
                String fina[][] = new String[index][2];
                fina[0][0] = outprint1[0][0];
                fina[0][1] = outprint1[0][1];
                int ri = 0;

                for (int cc = 0; cc < cu - 1; cc++) {
                        if (cc == 0) {
                                fina[ri][0] = outprint1[cc][0];
                                fina[ri][1] = outprint1[cc][1];

                                ri++;
                                continue;
                        }
                        if (!outprint1[cc + 1][0].equals(outprint1[cc][0])) {
                                fina[ri][0] = outprint1[cc + 1][0];
                                fina[ri][1] = outprint1[cc + 1][1];

                                ri++;
                        }
                }

                int record = 0;
        		String finas[][] = new String[index][4];
         		for (int cc = 0; cc < ri; cc++) {
         
         			if (out(fina[cc][0], Integer.parseInt(fina[cc][1]), N)) {
         				if (Integer.parseInt(fina[cc][1]) > 2)
        		
        				{	
        					finas[record][0] = fina[cc][0];
        					finas[record][1] = fina[cc][1];					
        					finas[record][2]=iden;
        					finas[record][3]=Integer.toString(identifier_count);        					
        					System.out.println("'" + finas[record][0] + "'" + finas[record][1]+ "/" + finas[record][2] + "," + finas[record][3]);
        					record++;
        				}
        			}
        		}
         		String sorted[][] = new String[record][4];
         		for (int cc = 0; cc < record; cc++) {
         			sorted[cc][0]=finas[cc][0];
         			sorted[cc][1]=finas[cc][1];
         			sorted[cc][2]=finas[cc][2];
         			sorted[cc][3]=finas[cc][3];
         		}
         		 Arrays.sort(sorted, new Comparator<String[]>() {
                     @Override
                     public int compare(final String[] entry1, final String[] entry2) {
                             
                    	 		final String time1 = entry1[0];
                             final String time2 = entry2[0];
                     
                             if (time1.length() > time2.length()) {
                                 return 1;
                              } else if (time1.length() < time2.length()) {
                                 return -1;
                              }
                              return time1.compareTo(time2);
                     }
             });
         		 
         		 String tempo[][][] = new String[record][record][4];
         		 int second=1;
                 int first=0;
              
         		for (int cc = 0; cc < record ; cc++) {
         		        String comp = sorted[cc][0];
         		        if(comp==null)
         		        	continue;
         		     
         		       tempo[first][0][0]=sorted[cc][0];
         		       tempo[first][0][1]=sorted[cc][1];
         		       tempo[first][0][2]=sorted[cc][2];
         		       tempo[first][0][3]=sorted[cc][3];
         		       
         		        for(int begin=cc+1;begin<record;begin++)
         		        {
         		        	if(sorted[begin][0]==null)
         		        		continue;
         		        	
         		        	if(sorted[begin][0].contains(comp))
         		        	{
         		        		tempo[first][second][0]=sorted[begin][0];
         		        		tempo[first][second][1]=sorted[begin][1];
         		        		tempo[first][second][2]=sorted[begin][2];
         		        		tempo[first][second][3]=sorted[begin][3];
         		        		sorted[begin][0]=null;
         		        		second++;
         		        	}
         		        }
         		        
         		   
         		       for(int begin=0;begin<second;begin++)
        		        {
         		    	   System.out.println(tempo[first][begin][0]);
        		        }
         		        second=1;
         		        first++;
         		} 
         		String sortagain[] = new String[first];
         		for(int cop=0;cop<first;cop++)
         		{
         			sortagain[cop]=tempo[cop][0][0];
         		}
         		
         		 Arrays.sort(sortagain);
         		for(int cop=0;cop<first;cop++)
         		{
         			String comp = sortagain[cop];
         			for(int last=0;last<first;last++)
         			{
         				if(comp.equals(tempo[last][0][0]))
         				{
         					for(int even=0;tempo[last][even][0]!=null;even++)
         					{
         						System.out.println("'" + tempo[last][even][0] + "'" + tempo[last][even][1]+ "/" + tempo[last][even][2] + "," + tempo[last][even][3]);               				
         					}
         					break;
         				}
         			}
         		}
         		
        }

        //check if the completed string has identifier
        public static boolean out(String input, int occur, int textlength) {
                int count_intext = 0;

                int count_inside = 0;
                String identifier;

                int M = input.length();

                SuffixArray temp = new SuffixArray(input);

                for (int c = 0; c < M; c++) {
                        for (int i = 1; i <= M - c; i++) {
                                count_intext = 0;
                                count_inside = 0;

                                identifier = input.substring(c, c + i);

                                for (int k = suffix.rank(identifier); k < textlength
                                                && suffix.select(k).startsWith(identifier); k++) {
                                        count_intext++;
                                }

                                for (int k = temp.rank(identifier); k < M
                                                && temp.select(k).startsWith(identifier); k++) {
                                        count_inside++;
                                }

                                if ((count_inside == 1) && ((count_intext / 2) < occur)
                                                && (identifier.length() != M)) {
                                        iden = identifier;
                                        identifier_count = count_intext;
                                        return true;

                                }
                        }
                }

                return false;
        }
        
        //read the input files
        static String readWithString(Reader fStream) throws IOException {

                BufferedReader br = new BufferedReader(fStream);
                String buffer, result = "";
                while ((buffer = br.readLine()) != null) {
                        result = result + buffer;
                }

                return result;
        }

}